# README #

XCode 6 project to compile Newton (newton-dynamics physics) as a library for iOs

### What is this repository for? ###

* By linking to this library, you can use newton in any iOs app and run it on iPhone / iPad
* Version 1.0 based on newton-dynamics master Sep 19, 2014 clones from https://github.com/MADEAPPS/newton-dynamics/commits/master

### How do I get set up? ###

* Clone the repository
* Open newton.xcodeproj
* Select target iOs Device
* Compile

All should be well, libnewton.a should have been created

### Use in own project ###
Link directly to the libnewton.a file and copy the newton.h file to your own project.

### Contribution guidelines ###

* You can reach me on bitbucket

### Who do I talk to? ###

* Repo owner or admin

### Credits ###

* All credit goes to the amazing work by Julio Jerez and Alain Suero on http://newtondynamics.com/.